from django.contrib.auth.models import User, Group
from rest_framework import viewsets, filters
from rest_framework import permissions
from rest_framework.decorators import api_view, authentication_classes, permission_classes

from .serializers import UserSerializer, GroupSerializer, MusicListSerializer, MusicSerializer, CategorySerializer, \
    AlbumSerializer, HistorySerializer
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from App.models import MusicList,Music,Category,Album,History
from App.pagination import CustomPagination


class MusicListViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = MusicList.objects.all()
    serializer_class = MusicListSerializer
    permission_classes = [permissions.IsAuthenticated]

class MusicViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    search_fields = ['title', 'author', 'album__title', 'album__categories__name']
    filter_backends = (filters.SearchFilter,)
    queryset = Music.objects.all()
    serializer_class = MusicSerializer
    permission_classes = [permissions.IsAuthenticated]

class CategoryViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    permission_classes = [permissions.IsAuthenticated]


class AlbumViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Album.objects.all()
    serializer_class = AlbumSerializer
    permission_classes = [permissions.IsAuthenticated]



class HistoryViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = History.objects.all()
    serializer_class = HistorySerializer
    permission_classes = [permissions.IsAuthenticated]
    pagination_class = CustomPagination


@api_view(['GET'])
@authentication_classes([SessionAuthentication, BasicAuthentication])
@permission_classes([IsAuthenticated])
def MusicListFavViewSet(request):
    user = request.user

    favlist = MusicList.objects.filter(user=user.id, type='favlist').first()
    serializer_context = {
        'request': request,
    }
    if not favlist :
        favlist = MusicList(user=user, type = "favlist", title="Liste des favoris", description="Votre liste des favoris")
        favlist.save()

    return Response(MusicListSerializer(favlist,context=serializer_context, many=False).data)

@api_view(['GET'])
@authentication_classes([SessionAuthentication, BasicAuthentication])
@permission_classes([IsAuthenticated])
def HistoryListViewSet(request):
    user = request.user

    history = History.objects.filter(user=user.id).order_by('-date')

    paginator = CustomPagination()
    result_page = paginator.paginate_queryset(history, request)
    print(result_page)
    serializer_context = {
        'request': request,
    }

    data = {}
    data['total'] = len(history)
    if request.GET.get('page') is not None:
        data['page'] = int(request.GET.get('page'))
    else:
        data['page'] = 1

    data['page_size'] = paginator.page_size
    data['results'] = HistorySerializer(result_page,context=serializer_context, many=True).data

    return Response(data)


@api_view(['GET'])
@authentication_classes([SessionAuthentication, BasicAuthentication])
@permission_classes([IsAuthenticated])
def MusicPlayListViewSet(request):
    user = request.user

    favlist = MusicList.objects.filter(user=user.id, type='playlist')
    serializer_context = {
        'request': request,
    }


    return Response(MusicListSerializer(favlist,context=serializer_context, many=True).data)

@api_view(['GET'])
@authentication_classes([SessionAuthentication, BasicAuthentication])
@permission_classes([IsAuthenticated])
def MusicGetViewSet(request, music_id):
    user = request.user

    music = Music.objects.filter(pk=music_id).first()

    favlist = MusicList.objects.filter(user=user.id, type='favlist').first()

    serializer_context = {
        'request': request,
    }
    data = MusicSerializer(music, context=serializer_context, many=False).data



    if not favlist:
        data['is_fav'] = False
    else:
        if music  in favlist.musics.all():
            data['is_fav'] = True
        else:
            data['is_fav'] = False


    return Response(data)

@api_view(['GET'])
@authentication_classes([SessionAuthentication, BasicAuthentication])
@permission_classes([IsAuthenticated])
def addMusicFavListView(request, music_id):
    user = request.user

    music = Music.objects.filter(pk=music_id).first()

    favlist = MusicList.objects.filter(user=user.id, type='favlist').first()

    serializer_context = {
        'request': request,
    }
    data = MusicSerializer(music, context=serializer_context, many=False).data

    if music:
        if not favlist:

            favlist = MusicList(user=user, type="favlist", title="Liste des favoris",
                                    description="Votre liste des favoris")
            favlist.save()

            favlist.musics.add(music)

        else:
            favlist.musics.add(music)


    return Response(data)

@api_view(['GET'])
@authentication_classes([SessionAuthentication, BasicAuthentication])
@permission_classes([IsAuthenticated])
def addMusicToHistory(request, music_id):
    user = request.user

    music = Music.objects.filter(pk=music_id).first()

    serializer_context = {
        'request': request,
    }
    data = MusicSerializer(music, context=serializer_context, many=False).data

    if music:
        if user:
            history = History(user=user, music=music)
            history.save()


    return Response(data)


@api_view(['GET'])
@authentication_classes([SessionAuthentication, BasicAuthentication])
@permission_classes([IsAuthenticated])
def removeMusicFavListView(request, music_id):
    user = request.user

    music = Music.objects.filter(pk=music_id).first()

    favlist = MusicList.objects.filter(user=user.id, type='favlist').first()

    serializer_context = {
        'request': request,
    }
    data = MusicSerializer(music, context=serializer_context, many=False).data


    if music:
        if favlist:
            favlist.musics.remove(music)


    return Response(data)




@api_view(['GET'])
@authentication_classes([SessionAuthentication, BasicAuthentication])
@permission_classes([IsAuthenticated])
def addMusicToListView(request, music_id,list_id):
    user = request.user

    music = Music.objects.filter(pk=music_id).first()

    list = MusicList.objects.filter(user=user.id, pk=list_id).first()

    serializer_context = {
        'request': request,
    }
    data = MusicSerializer(music, context=serializer_context, many=False).data


    if music:
        if list:
            list.musics.add(music)


    return Response(data)

@api_view(['GET'])
@authentication_classes([SessionAuthentication, BasicAuthentication])
@permission_classes([IsAuthenticated])
def removeMusicToListView(request, music_id,list_id):
    user = request.user

    music = Music.objects.filter(pk=music_id).first()

    list = MusicList.objects.filter(user=user.id, pk=list_id).first()


    serializer_context = {
        'request': request,
    }
    data = MusicSerializer(music, context=serializer_context, many=False).data

    if music:
        if list:
            list.musics.remove(music)


    return Response(data)

