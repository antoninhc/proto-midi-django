from django import forms

from . import models


class AddPlaylistForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(AddPlaylistForm, self).__init__(*args, **kwargs)

        for name in self.fields.keys():
            self.fields[name].widget.attrs.update({
                'class': 'form-control',
            })

    class Meta:
        model = models.MusicList
        fields = ['title', 'description', 'image']
