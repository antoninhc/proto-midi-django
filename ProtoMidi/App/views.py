from django.contrib.auth.models import User
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
# Create your views here.

from django.http import HttpResponse, JsonResponse
from django.contrib.auth.forms import UserCreationForm
from django.urls import reverse_lazy
from django.views import generic


from . import models
from . import forms


@login_required
def home(request):

  user = request.user


  add_playlist_form = forms.AddPlaylistForm()

  playlist = models.MusicList.objects.filter(user=user.id, type='playlist')


  return render(request, 'app/home.html', {'user': user,'all_playlist':playlist, 'add_playlist_form': add_playlist_form})


def addPlaylist(request):
  form = forms.AddPlaylistForm(data=request.POST, files=request.FILES)
  if request.is_ajax():
    if form.is_valid():

      playlist = form.save(commit=False)

      u = User.objects.get(id=int(request.POST.get('user_id')))
      playlist.user = u
      playlist.type = 'playlist'
      playlist.save()

      return JsonResponse({'playlist_id':playlist.id}, status=200)
  return JsonResponse({'data':''},status=400)



class SignUpView(generic.CreateView):
    form_class = UserCreationForm
    success_url = reverse_lazy('login')
    template_name = 'registration/register.html'