from django.urls import path
from . import  views

urlpatterns=[
  path('',views.home,name='home'),
  path('add_playlist',views.addPlaylist,name='add_playlist'),
  path('accounts/register/',views.SignUpView.as_view(),name='register_page'),
]