from django.contrib.auth.models import User
from django.db import models


# Create your models here.

class Category(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = "Categories"


class Album(models.Model):
    title = models.CharField(max_length=100)
    description = models.TextField(max_length=500)
    image = models.ImageField(upload_to='albumImg/', default='no-img.png')
    categories = models.ManyToManyField(Category, blank=True, related_name='albums')

    def __str__(self):
        return self.title


class Music(models.Model):
    title = models.CharField(max_length=100)
    author = models.CharField(max_length=100)
    songFile = models.FileField(blank=True, upload_to='musicFile/')
    album = models.ForeignKey(Album, on_delete=models.CASCADE, related_name='musics')
    minuteDuration = models.IntegerField(default=0)

    def __str__(self):
        return self.title

    @property
    def album_title(self):
        return self.album.title

    @property
    def album_image(self):
        return str(self.album.image)


class MusicList(models.Model):
    STATUS = (
        ('playlist', ('Playlist de musique')),
        ('favlist', ('Favoris')),
    )
    title = models.CharField(max_length=100)
    description = models.TextField(max_length=500)
    type = models.CharField(max_length=100, choices=STATUS, )
    image = models.ImageField(upload_to='musicListCover/', default='no-img.png')
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    musics = models.ManyToManyField(Music, blank=True, )

    def __str__(self):
        return self.title



class History(models.Model):

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    music = models.ForeignKey(Music, on_delete=models.CASCADE)
    date = models.DateTimeField(auto_now_add=True, blank=True)

    def __str__(self):
        return  self.music.title + " - " + str(self.date)

    class Meta:
        verbose_name_plural = "Histories"
