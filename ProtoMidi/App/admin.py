from django.contrib import admin

# Register your models here.
from .models import Music, MusicList, Album, Category, History

admin.site.register(Music)
admin.site.register(Album)
admin.site.register(MusicList)
admin.site.register(Category)
admin.site.register(History)

