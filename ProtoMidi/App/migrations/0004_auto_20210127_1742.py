# Generated by Django 3.1.5 on 2021-01-27 16:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('App', '0003_auto_20210127_1702'),
    ]

    operations = [
        migrations.AlterField(
            model_name='musiclist',
            name='type',
            field=models.CharField(choices=[('playlist', 'Playlist de musique'), ('favlist', 'Favoris')], max_length=100),
        ),
    ]
